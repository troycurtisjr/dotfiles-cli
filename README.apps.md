# My Apps Catch-all Configuration
This project tracks the various miscellaneous configuration for apps, both graphical and command-line, that do not warrant their own repository.

# Getting Started

This project expects to be rooted at the top-level of your home directory.  Generally it is a bad idea to have a git repo at the top-level of your home directory, so I use the [vcsh](https://github.com/RichiH/vcsh) project.  This allows git control of any files in your home directory without any of the symlink hacks that other options have.  If you don't want to adopt vcsh, then you should be able to grab a zip archive and extract it into your home directory.

Check out the `vcsh` instructions in my `dotfiles-usr` project.
